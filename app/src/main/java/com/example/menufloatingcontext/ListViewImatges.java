package com.example.menufloatingcontext;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class ListViewImatges extends AppCompatActivity {
    private ListView listViewImatges;
    private Integer[] imageIDs = {
            R.drawable.cat,
            R.drawable.duck,
            R.drawable.puppy,
    };
    CustomAdapter customAdapter = new CustomAdapter();


    String[] imageText = {
            "Cat",
            "Duck",
            "Puppy",
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view_imatges);
        listViewImatges = findViewById(R.id.listViewImatges);

        listViewImatges.setAdapter(customAdapter);
        registerForContextMenu(listViewImatges);

    }

    private class CustomAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return imageIDs.length;
        }

        @Override
        public Object getItem(int i) {
            return imageIDs[i];
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View v = getLayoutInflater().inflate(R.layout.row_data, null);
            TextView textRow = v.findViewById(R.id.textRow);
            ImageView imageRow = v.findViewById(R.id.imageRow);
            textRow.setText(imageText[i]);
            imageRow.setImageResource(imageIDs[i]);
            return v;
        }
    }
}